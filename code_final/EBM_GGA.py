from scipy.optimize import minimize
import numpy as np
import math
from random import choices
from matplotlib import pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering
from sklearn import metrics
from scipy.spatial.distance import cdist

def f(x, parametres, seuil=10**(-6)):
    """
    fonction de densité d'une gaussienne généralisée asymétrique
    :param x: float ou int
    :param parametres: list de 5 paramètres, paramètres = [alpha, lambda, gamma, mu]
    :return: f(x), la densité de probabilité au point x
    """
    alpha, lambd, gamma, mu = parametres
    delta = (2*(alpha**lambd)*(1-alpha)**lambd)/(alpha**lambd+(1-alpha)**lambd)
    A = (delta**(1/lambd))/(gamma**(1/lambd)*math.gamma(1+1/lambd))
    B = (-(delta/gamma)*(np.abs(mu-x))**lambd)
    if x < mu:
        p = A*np.e**(B/(alpha**lambd))
    else:
        p = A*np.e**(B/((1-alpha)**lambd))
    if p > seuil:
        return p
    else:
        return seuil


def f_array(X, parametres):
    """
    pareil que f mais avec des array
    :param X: array
    :param parametres:
    :return:
    """
    n = len(X)
    Y = np.zeros(n)
    for i in range(n):
        Y[i] = f(X[i], parametres)
    return Y


def generation(n, param, poids):
    population = [k for k in range(0, 256)]
    A = []
    for i in range(len(param)):
        weights = [f(j, param[i]) for j in range(0, 256)]
        A.append(choices(population, weights, k=int(poids[i]*n)))
    return A


def log_vraiss(theta, X):
    """
    return la log-vraissemblance de la fonction normale
    :param theta: theta = [alpha, lambda, gamma, mu]
    :param X: array
    :return:
    """
    n = len(X)
    alpha, lambd, gamma, mu = theta
    delta = (2*(alpha**lambd)*(1-alpha)**lambd)/(alpha**lambd+(1-alpha)**lambd)
    A = (delta**(1/lambd))/(gamma**(1/lambd)*math.gamma(1+1/lambd))

    log = n*np.log(A)
    for i in range(n):
        B = (-(delta/gamma)*(np.abs(mu-X[i]))**lambd)
        if X[i] < mu:
            C = B/(alpha**lambd)
        else:
            C = B/((1-alpha)**lambd)
        log += C
    return -log

def elbow_method(M, X):
    """
    Applique la méthode "du coude" pour choisir le nombre de clusters
    :param M: nombre maximal de clusters testé
    :param X: échantillon
    :return: retourne le nombre k de clusters le plus adapté au dataset
    """
    d = []
    K = range(1,M)

    for k in K:
        kmeanModel = KMeans(n_clusters=k).fit(X)
        kmeanModel.fit(X)
        
        #calcul la distorsion
        print("X shape :", X.shape)
        print("kmeanModel clusters centers", kmeanModel.cluster_centers_)
        d.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
    
    #calcul des cos des angles entre chaque segment de la courbe de distorsion
    angle_distortions=[]

    for i in range(1, len(d)-1):
        #calcul du produit scalaire divisé par les deux normes
        angle_distortions.append(((d[i-1]-d[i])*(d[i+1]-d[i]) - 1)/(math.sqrt((d[i-1]-d[i])**2 + 1)*math.sqrt((d[i+1]-d[i])**2 + 1)) )
    
    #on prend le cos le plus proche de 1 (le point de la courbe où l'angle est le plus aigu)
    return(angle_distortions.index(max(angle_distortions)) + 2 )

def init_EM(M, X, method="kmeans"):
    """
    initialise M_final lois GGA
    :param M: entier, nombre de classes maximal
    :param X: échantillon
    :param method: "kmeans" (par défaut) ou "spectral"
    :return: param, matrice de M_final lignes, et 5 colonnes: poids, alpha, lambda, gamma, mu
    """
    n = len(X)
    X_df = pd.DataFrame(X)
    means = []
    model = None
    M_final=elbow_method(M,X_df)
    print(M_final, " clusters trouvés.")

    clusters = [[] for k in range(M_final)]

    if method == "kmeans":
        model = KMeans(n_clusters=M_final, random_state=0).fit(X_df)
        means = model.cluster_centers_
    elif method == "spectral":
        model = SpectralClustering(n_clusters=M_final, assign_labels="kmeans", random_state=0).fit(X_df)

    labels = model.labels_
    # reconstruction des clusters
    for k in range(len(labels)):
        clusters[labels[k]].append(X[k])

    # on crée la matrice P qui donne l'appartenance de chaque pixel aux différentes classes:

    P = [M_final*[0] for i in range(n)]
    for i in range(n):
        P[i][labels[i]] = 1

    # calcul des moyennes pour le cas du spectral clustering
    if method == "spectral":
        for k in range(M_final):
            S = 0
            for e in clusters[k]:
                S += e
            means.append(S/len(clusters[k]))

    # il reste à calculer gamma
    gamma = [0]*M_final
    # calcul de gamma
    # inutile ici, ça se simplifie pour alpha=1/2 et lambda=2
    sigma = 1/4
    for i in range(M_final):
        l = len(clusters[i])
        K = 0
        mu = means[i][0]
        for x in clusters[i]:
            if x >= mu:
                K += (x-mu)
            else:
                K += (mu-x)
        gamma[i] = 2*K/l
        print(gamma[i])

    param = [[1/2, 2, gamma[i], means[i][0]] for i in range(M_final)]
    return param, P, M_final


# def init_EM(M, X, mu1, mu2, mu3, mu4):
#     """
#     initialise M lois GGA
#     :param M: entier, nombre de classe
#     :param X: échantillon
#     :return: param, matrice de M lignes, et 5 colonnes: poids, alpha, lambda, gamma, mu
#     """
#     param = [[0.5, 2, 2, mu1],
#              [0.5, 2, 2, mu2], [0.5, 2, 2, mu3], [0.5, 2, 2, mu4]]
#     return param


def etape_E_initiale(X, M, param):
    """
    Calcul de la matrice P, avec les proba que les x appartiennent à chaque classe
    :param X: echantillon
    :param M: nombre de classe
    :param param: paramètres initiaux
    :return: une matrice de taille len(X)*M avec pi,j la proba que xi vienne de la distribution fi
    """
    P = [M*[0] for i in range(len(X))]
    for i in range(len(X)):
        x = X[i]
        for j in range(M):
            P[i][j] = f(x, param[j])
    return P


def etape_E(X, M, param):
    """
    Calcul de la matrice P, avec les proba que les x appartiennent à chaque classe
    :param X: echantillon
    :param M: nombre de classe
    :param param: paramètres initiaux
    :return: une matrice de taille len(X)*M avec pi,j la proba que xi vienne de la distribution fi
    """
    P = [M*[0] for i in range(len(X))]
    for i in range(len(X)):
        x = X[i]
        tot = 0
        for j in range(M):
            P[i][j] = f(x, param[j])
            tot += P[i][j]
        for j in range(M):
            P[i][j] = P[i][j]/tot
    return P


def etape_B(X, M, param, P):
    """
    Etape B, attribue les pixels à une et unique classe, celle dont leur probabilité d'appartenir est la plus grande.
    :param X: Échantillon
    :param M: nombre de classe
    :param param: paramètres des lois
    :param P: Matrice des propabilité d'appartance de chaque point à chaque point (cf output etape_E)
    :return: matrice P, avec des 1 à la probabilité la plus importante.
    """
    n = len(X)
    for i in range(n):
        a = np.random.multinomial(20, P[i])
        P[i] = M*[0]
        P[i][np.argmax(a)] = 1
    return P


bnds = ((0.01, 0.99), (0.5, 3), (0, 10), (None, None))


def maxim(theta_0, X, n, methode):
    res = minimize(log_vraiss, theta_0, args=X, method=methode, bounds=((0.001, 0.99), (0.5, 3), (0, 10), (None, None)),
                   tol=1e-10, options={'disp': True})
    theta_f = res.x
    return theta_f


def etape_M(X, M, param, P):
    A = [[] for k in range(M)]
    for i in range(len(X)):
        for j in range(M):
            if P[i][j] == 1:
                A[j].append(X[i])
    for i in range(M):
        param[i] = maxim(param[i], A[i], 1000, methode)
    return param


def affiche(X, P):
    M = len(P[0])
    x = [[] for k in range(M)]
    for i in range(len(X)):
        x[P[i].index(1)-1].append(X[i])
    for j in range(M):
        print('j = {}'.format(j))
        plt.hist(x[j], 256, density=True, range=(0, 256))
    return None


def performance(M_init, P_init, P_final):
    """
    Donne la perforamnce de l'algorithme, taux de réussite. Attention P_init et P_final ne sont pas du même type
    :param P_init: Classe de chaque point initialement (attention différent du P de l'algo)
    :param P_f: Classe de chaque point à la fin (P en sortie de la dernière etape B
    :return: un taux en %, nombre de points bien prédits sur le nombre de point total
    """
    n = len(P_final) # nombre de points
    M_final = len(P_final[0])  # nombre de classes finale
    P_f = []
    for i in range(n):
        P_f.append(P_final[i].index(1))
    cor = np.zeros((M_init, M_final))
    print('P_f = {}'.format(P_f))
    print('len(P_f) = {}'.format(len(P_f)))
    for i in range(n):
        cor[P_init[i], P_f[i]] += 1
    print('cor = {}'.format(cor))
    correspondance = []
    for m in range(M_init):
        correspondance.append(list(cor[m]).index(cor[m].max()))
    print(correspondance)
    perf = [1*(correspondance[P_init[k]] == P_f[k]) for k in range(n)]
    return sum(perf)/n*100

def arret(P1, P2, param1, param2, k, m=0, crit=0.001):
    """fonction pour décider de l'arrêt des itérations
    :param P1 : une 1ere matrice P
    :param P2 : une 2eme matrice P
    :param param1 : une 1ere liste de liste de paramètres param
    :param param2 : une 2eme liste de liste de paramètres param
    :param k : nombre de différences maximales souhaitées dans les matrices P successives pour arrêter les itérations
    :param m : nombre de différences maximales souhaitées dans les listes de paramètres successives pour arrêter les itérations 
             (par défaut 0, on arrête quand les paramètres ne bougent plus)
    :param crit : norme maximale en dessous de laquelle deux listes de paramètres sont décidées égales (par défaut 10^(-3))
    """
    diff_P = 0
    n = len(P1)
    for i in range(n):
        if P1[i] != P2[i]:
            diff_P += 1
    
    l = len(param1)

    diff_param = 0
    for i in range(l):
        d = 0
        for j in range(4):
            d += abs(param1[i][j]-param2[i][j])/param1[i][j]

        if d >= crit:
            diff_param +=1

    return diff_P <= k and diff_param <= m

# test et boucle

lambd1 = 2
alpha1 = 0.5
gamma1 = 4
mu1 = 50
param1 = [alpha1, lambd1, gamma1, mu1]


lambd2 = 1
alpha2 = 0.4
gamma2 = 3
mu2 = 100
param2 = [alpha2, lambd2, gamma2, mu2]


lambd3 = 2
alpha3 = 0.3
gamma3 = 4
mu3 = 150
param3 = [alpha3, lambd3, gamma3, mu3]

lambd4 = 2
alpha4 = 0.8
gamma4 = 5
mu4 = 200
param4 = [alpha4, lambd4, gamma4, mu4]

param0 = [param1, param2, param3, param4]

poids = [0.3, 0.2, 0.4, 0.1]

# il faut n assez grand (plus que 10 000)

n = 20000
M = 4
methode = 'L-BFGS-B'
M_init = 4

X_1 = generation(n, param0, poids)

X_ech = []
P_initial = []
for i in range(len(X_1)):
    X_ech = X_ech + X_1[i]
    P_initial += len(X_1[i])*[i]

print('P_initial = {}'.format(P_initial))
print('len(P_initial) = {}'.format(len(P_initial)))
print('X_ech = {}'.format(X_ech))
print('len(X_ech) = {}'.format(len(X_ech)))


param, P0, M_final = init_EM(11, X_ech) #il va faire 10 kmeans pour un nombre de classes allant de 1 à 10 et choisir le plus adéquat avec la méthode du coude
print("Après initialisation : ")
print("param ", param)
print("P0 ", P0)
print("M_final ", M_final)

perf_init = performance(M_init, P_initial, P0)
print('perf_init = {}'.format(perf_init))
perf = []

k = 10  # A CHANGER
P_prec = P0
param_prec = param
i = 0

while(True):
    P0 = etape_E(X_ech, M_final, param)
    P = etape_B(X_ech, M_final, param, P0)
    param = etape_M(X_ech, M_final, param, P)
    perf.append(performance(M_init, P_initial, P))
    i+=1
    if not arret(P_prec, P, param_prec, param, k):
        P_prec=P
        param_prec=param
    else:
        break

perf_finale = performance(M_init, P_initial, P)
print('Perf_init = {}'.format(perf_init))
print('Perf_finale = {}'.format(perf_finale))
print('Perf = {}'.format(perf))

plt.subplot(211)
affiche(X_ech, P0)

plt.subplot(212)
affiche(X_ech, P)

#print(param)
x = np.linspace(0, 256, 1000)

plt.subplot(211)
for i in range(len(param)):
    y_0 = f_array(x, param0[i])
    plt.plot(x, y_0, label='Classe {} initial'.format(i+1))

plt.title('Classes initiales')

plt.subplot(212)
for i in range(len(param)):
    y_f = f_array(x, param[i])
    plt.plot(x, y_f, label='Final')

plt.title('Classes finales  Methode: {}, performance: {}%'.format(methode, round(perf_finale, 2)))

plt.legend()
plt.show()

print("Nombre d'étapes :", i)
