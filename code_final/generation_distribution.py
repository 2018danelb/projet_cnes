from random import choices
from matplotlib import pyplot as plt
import math
import numpy as np


def f(x, parametres, seuil=10**(-6)):
    """
    fonction de densité d'une gaussienne généralisée asymétrique
    :param x: float ou int
    :param parametres: list de 5 paramètres, paramètres = [alpha, lambda, gamma, mu]
    :return: f(x), la densité de probabilité au point x
    """
    alpha, lambd, gamma, mu = parametres
    delta = (2*(alpha**lambd)*(1-alpha)**lambd)/(alpha**lambd+(1-alpha)**lambd)
    A = (delta**(1/lambd))/(gamma**(1/lambd)*math.gamma(1+1/lambd))
    B = (-(delta/gamma)*(np.abs(mu-x))**lambd)
    if x < mu:
        p = A*np.e**(B/(alpha**lambd))
    else:
        p = A*np.e**(B/((1-alpha)**lambd))
    return max([p, seuil])


def f_array(X, parametres):
    """
    pareil que f mais avec des array
    :param X: array
    :param parametres:
    :return:
    """
    n = len(X)
    Y = np.zeros(n)
    for i in range(n):
        Y[i] = f(X[i], parametres)
    return Y


def f_tot(x, param, poids):
    """
    :param f: support de la fonction de prob
    :param param: dictionnaire, avec les paramètres de chaque fonction et leur poids
    :return: f_tot, la fonction de densité totale
    """
    m = len(param)
    p = 0
    for i in range(m):
        p += poids[i]*f(x, param[i])
    return p


def generation(n, param, poids):
    population = [k for k in range(0, 256)]
    A = []
    for i in range(len(param)):
        weights = [f(j, param[i]) for j in range(0, 256)]
        A.append(choices(population, weights, k=int(poids[i]*n)))
    return A


if __name__ == '__main__':
    """
    alpha doit être compris 0.01 et 0.99
    Lambda doit être compris entre 0.5 (voir 1) et 3
    Gamma ne doit pas dépasser 10   
    """
    poids1 = 0.4
    alpha1 = 0.8
    lambd1 = 2
    gamma1 = 5
    mu1 = 100
    param1 = [alpha1, lambd1, gamma1, mu1]

    poids2 = 0.6
    alpha2 = 0.45
    lambd2 = 1.5
    gamma2 = 8
    mu2 = 120
    param2 = [alpha2, lambd2, gamma2, mu2]

    poids3 = 0.4
    alpha3 = 3
    lambd3 = 8
    gamma3 = 4
    mu3 = 150
    param3 = [alpha3, lambd3, gamma3, mu3]

    poids4 = 0.1
    alpha4 = 0.8
    lambd4 = 2.5
    gamma4 = 5
    mu4 = 200
    param4 = [alpha4, lambd4, gamma4, mu4]

    param = [param1, param2]
    poids = [poids1, poids2]

    A = generation(20000, param, poids)

    for i in range(len(param)):
        plt.hist(A[i], 256, density=True, range=(0, 256))
        X = np.linspace(min(A[i]), max(A[i]), 1000)
        Y = f_array(X, param[i])
        plt.plot(X, Y, label='Alpha={}, lambda{}, gamma={}'.format(param[i][1], param[i][2], param[i][3]))
    plt.legend()
    plt.title('Génération des points avec la fonction choices')
    plt.show()
