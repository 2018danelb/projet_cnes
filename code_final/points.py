import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from generation_distribution import *


plt.clf()

X = [80, 80, 0, 0, 400, 275, 0, 0, 100, 100, 250, 100, 350, 350, 275, 290, 530, 500, 350, 600, 600, 400, 290, 350, 600,
     550, 530]
Y = [0, 125, 125, 0, 0, 125, 125, 200, 200, 125, 200, 200, 200, 125, 125, 110, 110, 200, 200, 200, 0, 0, 110, 50, 50,
     50, 110]


f1 = lambda x, y: (y < 125 and x<80)

f2 = lambda x, y: (x>=80 and y<125 and y < (-x + 400))

f3 = lambda x, y: (y >= (-x + 400) and y < 50)

f4 = lambda x, y: (y >= (-x + 400) and y >= 50 and y < 110 and y < (-3*x+1700))

f5 = lambda x, y: (y >= 125 and x < 100)

f6 = lambda x, y: (x >= 100 and y >= (0.5*x + 75))

f7 = lambda x, y: (y < (0.5*x + 75) and y >= 125 and x < 350 and x >= 100)

f8 = lambda x, y: (y >= (-x + 400) and y < (-3*x+1700) and y >= 110 and ((x < 350 and y < 125) or (x >= 350)))

f9 = lambda x, y: (y >= (-3*x+1700) and y >= 50)

longueur = 600
largeur = 200

points = np.zeros((longueur, largeur))
conflit = 0

for i in range(600):
    for j in range(200):
        if (1*f1(i, j) + 1*f2(i, j) + 1*f3(i, j) + 1*f4(i, j) + 1*f5(i, j) + 1*f6(i, j) + 1*f7(i, j) + 1*f8(i, j)
            + 1*f9(i, j)) >= 2:
            print(i, j)
            conflit += 1
        points[i, j] = 1*f1(i, j) + 2*f2(i, j) + 3*f3(i, j) + 4*f4(i, j) + 5*f5(i, j) + 6*f6(i, j) + 7*f7(i, j)\
                       + 8*f8(i, j) + 9*f9(i, j)

if conflit != 0:
    print('il y a {} conflit(s)'.format(conflit))

print(points)


def color(x):
    if x == 1: return'b'
    if x == 2: return'g'
    if x == 3: return'r'
    if x == 4: return'c'
    if x == 5: return'm'
    if x == 6: return'y'
    if x == 7: return'k'
    if x == 8: return'w'
    if x == 9: return'b'


print('ok')


q = 1

if q == 0:

    X = [[[], []], [[], []], [[], []], [[], []], [[], []], [[], []], [[], []], [[], []], [[], []]]

    k = 0

    for i in range(600):
        for j in range(200):
            k += 1
            X[int(points[i, j])-1][0].append(i)
            X[int(points[i, j])-1][1].append(j)

    print([(len(X[i][0]), len(X[i][1])) for i in range(9)])

    param = [[1, 0.5, 1, 1, (i+1)*250/10] for i in range(9)]

    plt.subplot(121)
    for i in range(9):
        print('etape {}'.format(i+1))
        X[i] += generation(len(X[i][0]), [param[i]])
        plt.hist(X[i][2], 256, density=True, range=(0, 256))

    print('X = {}'.format(X))

    heatmap = np.zeros((largeur, longueur))

    for i in range(9):
        print("dataframe etape {}".format(i))
        for j in range(len(X[i][0])):
            heatmap[X[i][1][j], X[i][0][j]] = X[i][2][j]
    plt.subplot(122)
    df_heat = pd.DataFrame(heatmap)

    sns.heatmap(df_heat)

    plt.show()

if q == 1:

    X = [[[], []], [[], []], [[], []], [[], []], [[], []], [[], []], [[], []], [[], []], [[], []]]

    print('X', X)
    print('Y', Y)
    print(X == Y)

    k = 0

    print(X[0])
    print(X[0][0])

    for i in range(600):
        for j in range(200):
            k += 1
            X[int(points[i, j])-1][0].append(i)
            X[int(points[i, j])-1][1].append(j)
            # print('Itération {}, X1: {}, X2: {}, X3: {}, X4: {}, X5: {}, X6: {}, X7: {}, X8: {}, X9: {}'.format(k, len(X[0][0]), len(X[1][0]), len(X[2][0]), len(X[3][0]), len(X[4][0]), len(X[5][0]), len(X[6][0]), len(X[7][0]), len(X[8][0])))

    print('k = {}'.format(k))

    print(np.array(X[2][0]), len(X[2][0]))
    print(np.array(X[2][1]), len(X[3][0]))
    print('X2 == X3: {}'.format(X[2][0] == X[3][0]))

    print('plt.scatter')
    # plt.scatter(np.array(X[2][0]), np.array(X[2][1]), color=color(2+1))

    for i in range(9):
        print(i, color(i+1))
        plt.scatter(np.array(X[i][0]), np.array(X[i][1]), color=color(i+1))
    plt.title('Définition des zones des classes (image de {} par {} pixels'.format(longueur, largeur))
    print('plt.show()')
    plt.show()
