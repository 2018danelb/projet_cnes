from scipy.optimize import minimize
import numpy as np
import math
from random import choices
from matplotlib import pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering
import Image
from Lecture_image import X_image


def f(x, parametres, seuil=10**(-6)):
    """
    fonction de densité d'une gaussienne généralisée asymétrique
    :param x: float ou int
    :param parametres: list de 5 paramètres, paramètres = [alpha, lambda, gamma, mu]
    :return: f(x), la densité de probabilité au point x
    """
    alpha, lambd, gamma, mu = parametres
    delta = (2*(alpha**lambd)*(1-alpha)**lambd)/(alpha**lambd+(1-alpha)**lambd)
    A = (delta**(1/lambd))/(gamma**(1/lambd)*math.gamma(1+1/lambd))
    B = (-(delta/gamma)*(np.abs(mu-x))**lambd)
    if x < mu:
        p = A*np.e**(B/(alpha**lambd))
    else:
        p = A*np.e**(B/((1-alpha)**lambd))
    return max([p, seuil])


def f_array(X, parametres):
    """
    pareil que f mais avec des array
    :param X: array
    :param parametres:
    :return:
    """
    n = len(X)
    Y = np.zeros(n)
    for i in range(n):
        Y[i] = f(X[i], parametres)
    return Y


def f_tot(x, param, poids):
    """
    :param f: support de la fonction de prob
    :param param: dictionnaire, avec les paramètres de chaque fonction et leur poids
    :return: f_tot, la fonction de densité totale
    """
    m = len(param)
    p = 0
    for i in range(m):
        p += poids[i]*f(x, param[i])
    return p


def f_array_tot(X, parametres, poids):
    """
    pareil que f mais avec des array
    :param X: array
    :param parametres:
    :return:
    """
    n = len(X)
    Y = np.zeros(n)
    for i in range(n):
        Y[i] = f_tot(X[i], parametres, poids)
    return Y


def generation(n, param, poids):
    population = [k for k in range(0, 256)]
    A = []
    for i in range(len(param)):
        weights = [f(j, param[i]) for j in range(0, 256)]
        A.append(choices(population, weights, k=int(poids[i]*n)))
    return A


def log_vraiss(theta, X):
    """
    return la log-vraissemblance de la fonction normale
    :param theta: theta = [alpha, lambda, gamma, mu]
    :param X: array
    :return:
    """
    n = len(X)
    alpha, lambd, gamma, mu = theta
    delta = (2*(alpha**lambd)*(1-alpha)**lambd)/(alpha**lambd+(1-alpha)**lambd)
    A = (delta**(1/lambd))/(gamma**(1/lambd)*math.gamma(1+1/lambd))

    log = n*np.log(A)
    for i in range(n):
        B = (-(delta/gamma)*(np.abs(mu-X[i]))**lambd)
        if X[i] < mu:
            C = B/(alpha**lambd)
        else:
            C = B/((1-alpha)**lambd)
        log += C
    return -log


def init_EM(M, X, method="kmeans"):
    """
    initialise M lois GGA
    :param M: entier, nombre de classe
    :param X: échantillon
    :param method: "kmeans" (par défaut) ou "spectral"
    :return: param, matrice de M lignes, et 5 colonnes: poids, alpha, lambda, gamma, mu
    """
    n = len(X)
    X_df = pd.DataFrame(X)
    means = []
    model = None
    clusters = [[] for k in range(M)]

    if method == "kmeans":
        model = KMeans(n_clusters=M, random_state=0).fit(X_df)
        means = model.cluster_centers_
    elif method == "spectral":
        model = SpectralClustering(n_clusters=M, assign_labels="kmeans", random_state=0).fit(X_df)

    labels = model.labels_
    # reconstruction des clusters
    for k in range(len(labels)):
        clusters[labels[k]].append(X[k])

    # on crée la matrice P qui donne l'appartenance de chaque pixel aux différentes classes:

    P = [M*[0] for i in range(n)]
    for i in range(n):
        P[i][labels[i]] = 1

    # calcul des moyennes pour le cas du spectral clustering
    if method == "spectral":
        for k in range(M):
            S = 0
            for e in clusters[k]:
                S += e
            means.append(S/len(clusters[k]))

    # il reste à calculer gamma
    gamma = [0]*M
    # calcul de gamma
    # inutile ici, ça se simplifie pour alpha=1/2 et lambda=2
    sigma = 1/4
    for i in range(M):
        l = len(clusters[i])
        K = 0
        mu = means[i]
        for x in clusters[i]:
            if x >= mu:
                K += (x-mu)
            else:
                K += (mu-x)
        gamma[i] = 2*K/l
        print(gamma[i])

    param = [[1/2, 2, gamma[i], means[i]] for i in range(M)]
    return param, P


# def init_EM(M, X, mu1, mu2, mu3, mu4):
#     """
#     initialise M lois GGA
#     :param M: entier, nombre de classe
#     :param X: échantillon
#     :return: param, matrice de M lignes, et 5 colonnes: poids, alpha, lambda, gamma, mu
#     """
#     param = [[0.5, 2, 2, mu1],
#              [0.5, 2, 2, mu2], [0.5, 2, 2, mu3], [0.5, 2, 2, mu4]]
#     return param


def etape_E_initiale(X, M, param):
    """
    Calcul de la matrice P, avec les proba que les x appartiennent à chaque classe
    :param X: echantillon
    :param M: nombre de classe
    :param param: paramètres initiaux
    :return: une matrice de taille len(X)*M avec pi,j la proba que xi vienne de la distribution fi
    """
    P = [M*[0] for i in range(len(X))]
    for i in range(len(X)):
        x = X[i]
        for j in range(M):
            P[i][j] = f(x, param[j])
    return P


def etape_E(X, M, param):
    """
    Calcul de la matrice P, avec les proba que les x appartiennent à chaque classe
    :param X: echantillon
    :param M: nombre de classe
    :param param: paramètres initiaux
    :return: une matrice de taille len(X)*M avec pi,j la proba que xi vienne de la distribution fi
    """
    P = [M*[0] for i in range(len(X))]
    for i in range(len(X)):
        x = X[i]
        tot = 0
        for j in range(M):
            P[i][j] = f(x, param[j])
            tot += P[i][j]
        for j in range(M):
            P[i][j] = P[i][j]/tot
    return P


def etape_B(X, M, P):
    """
    Etape B, attribue les pixels à une et unique classe, celle dont leur probabilité d'appartenir est la plus grande.
    :param X: Échantillon
    :param M: nombre de classe
    :param param: paramètres des lois
    :param P: Matrice des propabilité d'appartance de chaque point à chaque point (cf output etape_E)
    :return: matrice P, avec des 1 à la probabilité la plus importante.
    """
    n = len(X)
    for i in range(n):
        a = np.random.multinomial(20, P[i])
        P[i] = M*[0]
        P[i][np.argmax(a)] = 1
    return P


bnds = ((0.01, 0.99), (0.5, 3), (0, 10), (None, None))


def maxim(theta_0, X, methode):
    res = minimize(log_vraiss, theta_0, args=X, method=methode, bounds=((0.001, 0.99), (0.5, 3), (0, 10), (None, None)),
                   tol=None, options={'disp': True, 'ftol': 1e-3})
    theta_f = res.x
    return theta_f


def etape_M(X, M, param, P):
    A = [[] for k in range(M)]
    for i in range(len(X)):
        for j in range(M):
            if P[i][j] == 1:
                A[j].append(X[i])
    for i in range(M):
        print("\n \n----- Classe {} -----\n \n".format(i+1))
        param[i] = maxim(param[i], A[i], methode)
    return param


def affiche(X, P, title=None):
    M = len(P[0])
    x = [[] for k in range(M)]
    for i in range(len(X)):
        x[P[i].index(1)].append(X[i])
    for j in range(M):
        print('j = {}'.format(j))
        plt.hist(x[j], 256, density=True, range=(0, 256))
    plt.title(title)
    return None


def affiche_2(X, P, title=None):
    M = len(P[0])
    x = [[] for k in range(M)]
    for i in range(len(X)):
        x[P[i].index(1)].append(X[i])
    for j in range(M):
        x[j] = np.array(x[j])
    plt.hist(x, 256, density=True, range=(0, 256), histtype='barstacked')
    plt.title(title)
    return None


def performance(P_init, P_final):
    """
    Donne la perforamnce de l'algortihme, taux de réussite. Attention P_init et P_final ne sont pas du même type
    :param P_init: Classe de chaque point initialement (attention différent du P de l'algo)
    :param P_f: Classe de chaque point à la fin (P en sortie de la dernière etape B
    :return: un taux en %, nombre de points bien prédits sur le nombre de point total
    """
    n = len(P_final)  # nombre de points
    M = len(P_final[0])  # nombre de classes
    P_f = []
    for i in range(n):
        P_f.append(P_final[i].index(1))
    cor = np.zeros((M, M))
    print('P_f = {}'.format(P_f))
    print('len(P_f) = {}'.format(len(P_f)))
    for i in range(n):
        cor[P_init[i], P_f[i]] += 1
    print('cor = {}'.format(cor))
    correspondance = []
    for m in range(M):
        correspondance.append(list(cor[m]).index(cor[m].max()))
    print(correspondance)
    perf = [1*(correspondance[P_init[k]] == P_f[k]) for k in range(n)]
    return sum(perf)/n*100


def arret(P1, P2, param1, param2, k, m=3, crit=0.001):
    """fonction pour décider de l'arrêt des itérations
    :param P1 : une 1ere matrice P
    :param P2 : une 2eme matrice P
    :param param1 : une 1ere liste de liste de paramètres param
    :param param2 : une 2eme liste de liste de paramètres param
    :param k : nombre de différences maximales souhaitées dans les matrices P successives pour arrêter les itérations
    :param m : nombre de différences maximales souhaitées dans les listes de paramètres successives pour arrêter les itérations
             (par défaut 0, on arrête quand les paramètres ne bougent plus)
    :param crit : norme maximale en dessous de laquelle deux listes de paramètres sont décidées égales (par défaut 10^(-3))
    """
    diff_P = 0
    n = len(P1)
    for i in range(n):
        if P1[i] != P2[i]:
            diff_P += 1

    l = len(param1)
    diff_param = 0
    for i in range(l):
        d = 0
        for j in range(4):
            d += abs(param1[i][j]-param2[i][j])/param1[i][j]
        if d >= crit:
            diff_param += 1

    return diff_P <= k and diff_param <= m


# test et boucle

lambd1 = 2
alpha1 = 0.5
gamma1 = 4
mu1 = 80
param1 = [alpha1, lambd1, gamma1, mu1]


lambd2 = 1
alpha2 = 0.4
gamma2 = 3
mu2 = 100
param2 = [alpha2, lambd2, gamma2, mu2]


lambd3 = 2
alpha3 = 0.3
gamma3 = 4
mu3 = 180
param3 = [alpha3, lambd3, gamma3, mu3]

lambd4 = 2
alpha4 = 0.8
gamma4 = 5
mu4 = 210
param4 = [alpha4, lambd4, gamma4, mu4]

param0 = [param1, param2, param3, param4]

poids = [0.3, 0.2, 0.4, 0.1]

# il faut n assez grand (plus que 10 000)

n = 20000
M = len(param0)
methode = 'L-BFGS-B'

# X_1 = generation(n, param0, poids)
# X_1, Pos = Image.generation_image(param0)
#
#
#
# X_ech = []
# P_initial = []
# for i in range(M):
#     X_ech = X_ech + X_1[i]
#     P_initial += len(X_1[i])*[i]

#
# print('P_initial = {}'.format(P_initial))
# print('len(P_initial) = {}'.format(len(P_initial)))
# print('X_ech = {}'.format(X_ech))
# print('len(X_ech) = {}'.format(len(X_ech)))

file = "image1.png"
X_ech, shape = X_image(file)
P_initial = len(X_ech)*[1]

M = 5
n = len(X_ech)

param, P0 = init_EM(M, X_ech)

affiche_2(X_ech, P0, title='initialisation')
plt.savefig('Image_1/initialisation')
# plt.show()

perf_init = performance(P_initial, P0)
print('perf_init = {}'.format(perf_init))
perf = []

k = 300  # A CHANGER
P_prec = P0
param_prec = param
i = 0

while True:
    i += 1
    P0 = etape_E(X_ech, M, param)
    P = etape_B(X_ech, M, P0)
    print("\n \n --------------- Etape M n°{} ---------------\n \n".format(i))
    param = etape_M(X_ech, M, param, P)
    perf.append(performance(P_initial, P))

    if arret(P_prec, P, param_prec, param, k):
        break
    if i == 4:
        break
    else:
        plt.clf()
        P_prec = P
        param_prec = param
        # Image.affichage_image(P, Pos, 'iteration_{}'.format(i))
        affiche_2(X_ech, P, title='etape {}'.format(i))
        poids = M*[0]
        for j in range(n):
            poids[int(P[j].index(1))] += 1
        for j in range(M):
            poids[j] /= n
        x = np.linspace(0, 256, 1000)
        y = f_array_tot(x, param, poids)
        plt.plot(x, y)
        plt.savefig('Image_1/etape_{}'.format(i))
        # plt.show()
        print(param)


perf_finale = performance(P_initial, P)
print('Perf_init = {}'.format(perf_init))
print('Perf_finale = {}'.format(perf_finale))
print('Perf = {}'.format(perf))

plt.subplot(211)
affiche(X_ech, P0)

plt.subplot(212)
affiche(X_ech, P)

print(param)
plt.clf()
poids = M*[0]
for j in range(n):
    poids[int(P[j].index(1))] += 1
for j in range(M):
    poids[j] /= n
x = np.linspace(0, 256, 1000)
y = f_array_tot(x, param, poids)
affiche_2(X_ech, P, title='etape {}'.format(i))
plt.legend('parametres = {}'.format(param))
plt.savefig('Image_1/etape_finale'.format(i))

print("Nombre d'étapes :", i)

X_classe = [int(P[k].index(1)*255/M) for k in range(len(X_ech))]

image_finale = np.zeros(shape)
for k in range(shape[0]):
    image_finale[k] = np.array(X_classe[k*shape[1]:(k+1)*shape[1]])

print('image_finale = {}'.format(image_finale))
plt.imshow(image_finale)
plt.savefig('Image_1/Image_finale')

# Image.affichage_image(P, Pos, 'final')

