import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from generation_distribution import *



def color(x):
    if x == 1: return'b'
    if x == 2: return'g'
    if x == 3: return'r'
    if x == 4: return'c'
    if x == 5: return'm'
    if x == 6: return'y'
    if x == 7: return'k'
    if x == 8: return'w'
    if x == 9: return'b'



def generation_image(param):
    """
    
    :param param: 
    :return: 
    """
    plt.clf()

    f1 = lambda x, y: (y < 125 and x < 80)
    f2 = lambda x, y: (x >= 80 and y < 125 and y < (-x + 400))
    f3 = lambda x, y: ((-x + 400) <= y < 50)
    f4 = lambda x, y: ((-x + 400) <= y < 110 and 50 <= y < (-3 * x + 1700))
    f5 = lambda x, y: (y >= 125 and x < 100)
    f6 = lambda x, y: (x >= 100 and y >= (0.5*x + 75))
    f7 = lambda x, y: ((0.5 * x + 75) > y >= 125 and 350 > x >= 100)
    f8 = lambda x, y: ((-x + 400) <= y < (-3 * x + 1700) and y >= 110 and ((x < 350 and y < 125) or (x >= 350)))
    f9 = lambda x, y: (y >= (-3*x+1700) and y >= 50)

    longueur = 600
    largeur = 200
    points = np.zeros((longueur, largeur))
    conflit = 0

    M = len(param)
    classes = [k+1 for k in range(M)]+[np.random.randint(1, M+1) for k in range(9-M)]

    print(classes)

    for i in range(600):
        for j in range(200):
            if (1*f1(i, j) + 1*f2(i, j) + 1*f3(i, j) + 1*f4(i, j) + 1*f5(i, j) + 1*f6(i, j) + 1*f7(i, j) + 1*f8(i, j)
            + 1*f9(i, j)) >= 2:
                print(i, j)
                conflit += 1
            points[i, j] = classes[0]*f1(i, j) + classes[1]*f2(i, j) + classes[2]*f3(i, j) + classes[3]*f4(i, j)\
                           + classes[4]*f5(i, j) + classes[5]*f6(i, j) + classes[6]*f7(i, j)\
                           + classes[7]*f8(i, j) + classes[8]*f9(i, j)

    if conflit != 0:
        print('il y a {} conflit(s)'.format(conflit))

    # print(points)
    q = 0

    X = []
    for k in range(M):
        X.append([[], []])

    for i in range(600):
        for j in range(200):
            X[int(points[i, j])-1][0].append(i)
            X[int(points[i, j])-1][1].append(j)

    n = 600*200
    poids = [len(X[i][0])/n for i in range(M)]

    Pos = np.zeros((n, 2))
    q = 0
    for m in range(M):
        for k in range(len(X[m][0])):
            Pos[q] = [int(X[m][0][k]), int(X[m][1][k])]
            q += 1

    plt.subplot(121)
    A = generation(n, param, poids)
    for i in range(M):
        print('etape {}'.format(i+1))
        X[i] += [A[i]]
        plt.hist(X[i][2], 256, density=True, range=(0, 256))

    heatmap = np.zeros((largeur, longueur))

    for i in range(M):
        print("dataframe etape {}".format(i))
        for j in range(len(X[i][0])):
            heatmap[199-X[i][1][j], X[i][0][j]] = X[i][2][j]

    plt.subplot(122)
    df_heat = pd.DataFrame(heatmap)

    sns.heatmap(df_heat)
    plt.savefig('Images/image')
#    plt.show()

    plt.clf()
    print('plt.scatter')
    for i in range(M):
        print(i, color(i+1))
        plt.scatter(np.array(X[i][0]), np.array(X[i][1]), color=color(i+1))
    plt.title('Définition des zones des {} classes'.format(M))
    plt.savefig('Images/Classe_initial')
    print('plt.show()')
#    plt.show()

    return A, Pos


def affichage_image(P, Pos, text):
    """
    affiche l'écran avec les classes obtenues
    :param P: matrice P de l'attribution des points aux différentes classes, à la fin du process
    :param Pos: liste des position de chaque point
    :return: dessine l'image avec les nouvelles classes coloriées
    """
    print('affichage image, len(P) = {}, len(Pos) = {}'.format(len(P), len(Pos)))
    n = len(P)
    M = len(P[0])
    plt.clf()
    print('plt.scatter')
    classes = np.zeros(n)

    for i in range(n):
        classes[i] = P[i].index(1)

    X = []
    for k in range(M):
        X.append([[], []])
    for i in range(n):
        X[int(classes[i])][0].append(Pos[i][0])
        X[int(classes[i])][1].append(Pos[i][1])

    plt.clf()
    print('plt.scatter')
    for i in range(M):
        print(i, color(i+1))
        plt.scatter(np.array(X[i][0]), np.array(X[i][1]), color=color(i+1))
    plt.title('Résultats des {} classes'.format(M))
    plt.savefig('Images/Classes_'+text)
    return None


if __name__ == '__main__':
    lambd1 = 2
    alpha1 = 0.5
    gamma1 = 4
    mu1 = 40
    param1 = [alpha1, lambd1, gamma1, mu1]

    lambd2 = 1
    alpha2 = 0.4
    gamma2 = 3
    mu2 = 100
    param2 = [alpha2, lambd2, gamma2, mu2]

    lambd3 = 2
    alpha3 = 0.3
    gamma3 = 4
    mu3 = 180
    param3 = [alpha3, lambd3, gamma3, mu3]

    lambd4 = 2
    alpha4 = 0.8
    gamma4 = 5
    mu4 = 220
    param4 = [alpha4, lambd4, gamma4, mu4]

    param0 = [param1, param2, param3, param4]

    X_1, Pos = generation_image(param0)
    M = len(param0)

    X_ech = []
    P_initial = []
    for i in range(len(X_1)):
        p = M*[0]
        p[i] = 1
        X_ech = X_ech + X_1[i]
        P_initial += len(X_1[i])*[p]
    affichage_image(P_initial, Pos, 'initial')
