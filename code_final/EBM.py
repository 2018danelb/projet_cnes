
# coding: utf-8

# # Projet Cnes

# #### Importation des librairies

# In[10]:


from scipy.optimize import minimize
import numpy as np
import math
from random import choices
from matplotlib import pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering
import matplotlib.image as mpimg
from sklearn import metrics
from scipy.spatial.distance import cdist


# ## Définition des lois et création des points

# #### Densité de probabilité d'une loi GGA (pour un scalaire et un array)

# In[11]:


def f(x, parametres, seuil=10**(-6)):
    """
    fonction de densité d'une gaussienne généralisée asymétrique
    :param x: float ou int
    :param parametres: list de 5 paramètres, paramètres = [alpha, lambda, gamma, mu]
    :return: f(x), la densité de probabilité au point x
    """
    alpha, lambd, gamma, mu = parametres
    delta = (2*(alpha**lambd)*(1-alpha)**lambd)/(alpha**lambd+(1-alpha)**lambd)
    A = (delta**(1/lambd))/(gamma**(1/lambd)*math.gamma(1+1/lambd))
    B = (-(delta/gamma)*(np.abs(mu-x))**lambd)
    if x < mu:
        p = A*np.e**(B/(alpha**lambd))
    else:
        p = A*np.e**(B/((1-alpha)**lambd))
    return max([p, seuil])

def f_array(X, parametres):
    """
    pareil que f mais avec des array
    :param X: array
    :param parametres:
    :return:
    """
    n = len(X)
    Y = np.zeros(n)
    for i in range(n):
        Y[i] = f(X[i], parametres)
    return Y


def f_tot(x, param, poids):
    """
    fonction de répartition totale, avec les différentes classes
    :param f: support de la fonction de prob
    :param param: dictionnaire, avec les paramètres de chaque fonction et leur poids
    :return: f_tot, la fonction de densité totale
    """
    m = len(param)
    p = 0
    for i in range(m):
        p += poids[i]*f(x, param[i])
    return p


def f_array_tot(X, parametres, poids):
    """
    pareil que f_tot mais avec des array
    :param X: array
    :param parametres:
    :return:
    """
    n = len(X)
    Y = np.zeros(n)
    for i in range(n):
        Y[i] = f_tot(X[i], parametres, poids)
    return Y


# #####  Fonction d'importation de l'image

# In[12]:


def X_image(file):
    """

    :param file:
    :return:
    """
    img = mpimg.imread(file)

    if img.dtype == np.float32:  # Si le résultat n'est pas un tableau d'entiers
        img = (img * 255).astype(np.uint8)

    plt.imshow(img)
    plt.show()
    #
    # print(img)
    # print(img.shape)

    X = []
    for i in range(img.shape[0]):
        X += list(img[i])

    # print(X)
    # print(len(X))
    return X, img.shape


# #### Importation de l'image

# In[13]:


# file = "../image2.png"
# X_ech, shape = X_image(file)
# plt.hist(X_ech, 256, density=True, range=(0, 256), histtype='barstacked')
# plt.title('Répartition des points')
# plt.savefig('../Resultats/Image_2/repartition_points')
# plt.show()
# n = len(X_ech)


# ## Mise en place de l'algorithme EBM

# #### Fonction de la log-vraissemblance

# In[14]:


def log_vraiss(theta, X):
    """
    return la log-vraissemblance de la fonction normale
    :param theta: theta = [alpha, lambda, gamma, mu]
    :param X: array
    :return:
    """
    n = len(X)
    alpha, lambd, gamma, mu = theta
    delta = (2*(alpha**lambd)*(1-alpha)**lambd)/(alpha**lambd+(1-alpha)**lambd)
    A = (delta**(1/lambd))/(gamma**(1/lambd)*math.gamma(1+1/lambd))

    log = n*np.log(A)
    for i in range(n):
        B = (-(delta/gamma)*(np.abs(mu-X[i]))**lambd)
        if X[i] < mu:
            C = B/(alpha**lambd)
        else:
            C = B/((1-alpha)**lambd)
        log += C
    return -log


# #### Fonction d'affichage des données

# In[15]:


def affiche_2(X, P, title=None):
    M = len(P[0])
    x = [[] for k in range(M)]
    for i in range(len(X)):
        x[P[i].index(1)].append(X[i])
    for j in range(M):
        x[j] = np.array(x[j])
    plt.hist(x, 256, density=False,range=(0, 256), histtype='barstacked')
    plt.title(title)
    return None


# #### Fonction d'écriture des paramètres

# In[16]:


def affichage_param(param):
    for i in range(len(param)):
        print('Classe {0}: alpha= {1}, lambda= {2}, gamma= {3}, mu= {4}'.format(i+1, param[i][0], param[i][1], param[i][2], param[i][3]))


# #### Fonction d'initialisation

# In[17]:


def elbow_method(M, X, N=2):
    """
    Applique la méthode "du coude" pour choisir le nombre de clusters
    :param M: nombre maximal de clusters testés
    :param N: nombre minimal de clusters renvoyés
    :param X: échantillon
    :return: retourne le nombre k de clusters le plus adapté au dataset
    """
    d = []
    K = range(1,M+1)

    for k in K:
        kmeanModel = KMeans(n_clusters=k).fit(X)
        kmeanModel.fit(X)
        
        #calcul la distorsion
        d.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
    plt.plot(K, d, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Distortion')
    plt.title('Elbow Method')
    plt.show()

    #calcul des cos des angles entre chaque segment de la courbe de distorsion
    angle_distortions=[]

    for i in range(1, len(d)-1):
        #calcul du produit scalaire divisé par les deux normes
        angle_distortions.append(((d[i-1]-d[i])*(d[i+1]-d[i]) - 1)/(math.sqrt((d[i-1]-d[i])**2 + 1)*math.sqrt((d[i+1]-d[i])**2 + 1)) )
    
    #on prend le cos le plus proche de 1 (le point de la courbe où l'angle est le plus aigu)
    k = angle_distortions.index(max(angle_distortions[(N-2):])) + 2

    print("Elbow Method - Number of clusters ", k)

    return(k)

def init_EM(M, X, method="kmeans", M_min=2):
    """
    initialise M_final lois GGA
    :param M: entier, nombre de classes maximal
    :param Nmin: entier, nombre de classes minimal
    :param X: échantillon
    :param method: "kmeans" (par défaut) ou "spectral"
    :return: param, matrice de M_final lignes, et 5 colonnes: poids, alpha, lambda, gamma, mu
    """
    n = len(X)
    X_df = pd.DataFrame(X)
    means = []
    model = None
    M_final=elbow_method(M,X_df, N=M_min)
    print(M_final, " clusters trouvés.")

    clusters = [[] for k in range(M_final)]

    if method == "kmeans":
        model = KMeans(n_clusters=M_final, random_state=0).fit(X_df)
        means = model.cluster_centers_
    elif method == "spectral":
        model = SpectralClustering(n_clusters=M_final, assign_labels="kmeans", random_state=0).fit(X_df)

    labels = model.labels_
    # reconstruction des clusters
    for k in range(len(labels)):
        clusters[labels[k]].append(X[k])

    # on crée la matrice P qui donne l'appartenance de chaque pixel aux différentes classes:

    P = [M_final*[0] for i in range(n)]
    for i in range(n):
        P[i][labels[i]] = 1

    # calcul des moyennes pour le cas du spectral clustering
    if method == "spectral":
        for k in range(M_final):
            S = 0
            for e in clusters[k]:
                S += e
            means.append(S/len(clusters[k]))

    # il reste à calculer gamma
    gamma = [0]*M_final
    # calcul de gamma
    # inutile ici, ça se simplifie pour alpha=1/2 et lambda=2
    sigma = 1/4
    for i in range(M_final):
        l = len(clusters[i])
        K = 0
        mu = means[i][0]
        for x in clusters[i]:
            if x >= mu:
                K += (x-mu)
            else:
                K += (mu-x)
        gamma[i] = 2*K/l
        print(gamma[i])

    param = [[1/2, 2, gamma[i], means[i][0]] for i in range(M_final)]
    return param, P, M_final


# In[18]:


# M = 7 # nombres de classes
# param, P0 = init_EM(M, X_ech)
# affichage_param(param)
# affiche_2(X_ech, P0, title='initialisation')
# poids = M*[0]
# for j in range(n):
#     poids[int(P0[j].index(1))] += 1
# for j in range(M):
#     poids[j] /= n
# x = np.linspace(0, 256, 1000)
# y = f_array_tot(x, param, poids)
# plt.plot(x, y)
# plt.savefig('../Resultats/Image_2/initialisation')
# plt.show()


# #### Fonction de l'étape E

# In[19]:


def etape_E(X, M, param):
    """
    Calcul de la matrice P, avec les proba que les x appartiennent à chaque classe
    :param X: echantillon
    :param M: nombre de classe
    :param param: paramètres initiaux
    :return: une matrice de taille len(X)*M avec pi,j la proba que xi vienne de la distribution fi
    """
    P = [M*[0] for i in range(len(X))]
    for i in range(len(X)):
        x = X[i]

        tot = 0
        for j in range(M):
            P[i][j] = f(x, param[j])
            tot += P[i][j]
        for j in range(M):
            P[i][j] = P[i][j]/tot
    return P


# #### Fonction de l'étape B

# In[20]:


def etape_B(X, M, P):
    """
    Etape B, attribue les pixels à une et unique classe, celle dont leur probabilité d'appartenir est la plus grande.
    :param X: Échantillon
    :param M: nombre de classe
    :param param: paramètres des lois
    :param P: Matrice des propabilité d'appartance de chaque point à chaque point (cf output etape_E)
    :return: matrice P, avec des 1 à la probabilité la plus importante.
    """
    n = len(X)
    for i in range(n):
        a = np.random.multinomial(20, P[i])
        P[i] = M*[0]
        P[i][np.argmax(a)] = 1
    return P


# #### fonction de l'étape M

# In[21]:


def etape_M(X, M, param, P, methode='L-BFGS-B'):
    A = [[] for k in range(M)]
    for i in range(len(X)):
        for j in range(M):
            if P[i][j] == 1:
                A[j].append(X[i])
    for i in range(M):
        # print("\n \n----- Classe {} -----\n \n".format(i+1))
        param[i] = maxim(param[i], A[i], methode)
    return param


# #### Fonction de maximisation de la log-vraissemblance

# In[22]:


def maxim(theta_0, X, methode='L-BFGS-B'):
    res = minimize(log_vraiss, theta_0, args=X, method=methode, bounds=((0.001, 0.99), (0.5, 3), (0, 10), (None, None)),
                   tol=None, options={'disp': True, 'ftol': 1e-3})
    theta_f = res.x
    return theta_f


# #### Fonction d'arrêt de l'EBM

# In[23]:


def arret(P1, P2, param1, param2, k, m=6, crit=0.01):
    """fonction pour décider de l'arrêt des itérations
    :param P1 : une 1ere matrice P
    :param P2 : une 2eme matrice P
    :param param1 : une 1ere liste de liste de paramètres param
    :param param2 : une 2eme liste de liste de paramètres param
    :param k : nombre de différences maximales souhaitées dans les matrices P successives pour arrêter les itérations
    :param m : nombre de différences maximales souhaitées dans les listes de paramètres successives pour arrêter les itérations
             (par défaut 0, on arrête quand les paramètres ne bougent plus)
    :param crit : norme maximale en dessous de laquelle deux listes de paramètres sont décidées égales (par défaut 10^(-3))
    """
    diff_P = 0
    n = len(P1)
    for i in range(n):
        if P1[i] != P2[i]:
            diff_P += 1

    l = len(param1)
    diff_param = 0
    for i in range(l):
        d = 0
        for j in range(4):
            d += abs(param1[i][j]-param2[i][j])/param1[i][j]
        if d >= crit:
            diff_param += 1
    print('diff_P = {}, diff_param = {}'.format(diff_P, diff_param))

    return diff_P <= k and diff_param <= m

def EBM(X_ech, M_max=10, k=3500, m=6, crit=0.01, M_min=2, iter_min=3, iter_max=100):
    n=len(X_ech)

    param, P0, M = init_EM(M_max, X_ech, M_min=M_min)
    affichage_param(param)
    affiche_2(X_ech, P0, title='initialisation')
    poids = M*[0]

    for j in range(n):
        poids[int(P0[j].index(1))] += 1
    for j in range(M):
        poids[j] /= n
    x = np.linspace(0, 256, 1000)
    y = f_array_tot(x, param, poids)
    plt.plot(x, y)
    plt.savefig('../Resultats/Image_2/initialisation')
    plt.show()

    # ### Réalisation de l'algorithme sur les données

    # In[24]:


    methode = 'L-BFGS-B'
    #k = 3500  #  Nombre de points qui doivent changer pour la fonction d'arrêt
    P_prec = P0
    param_prec = param[:]
    i = 0

    while i<iter_max:
        i += 1
        P0 = etape_E(X_ech, M, param)
        P = etape_B(X_ech, M, P0)
        print("\n \n --------------- Etape M n°{} ---------------\n ".format(i))
        param = etape_M(X_ech, M, param, P)
        if i>= iter_min and arret(P_prec, P, param_prec, param, k, m=m, crit=crit):
            break
        else:
            plt.clf()
            P_prec = P
            param_prec = param[:]
            affiche_2(X_ech, P, title='etape {}'.format(i))
            poids = M*[0]
            for j in range(n):
                poids[int(P[j].index(1))] += 1
            for j in range(M):
                poids[j] /= n
            x = np.linspace(0, 256, 1000)
            y = f_array_tot(x, param, poids)
            plt.plot(x, y)
            plt.savefig('../Resultats/Image_2/etape_{}'.format(i))
            #plt.show()
            affichage_param(param)


    # ## Affichage des résultats

    # In[25]:


    affichage_param(param)
    plt.clf()
    poids = M*[0]
    for j in range(n):
        poids[int(P[j].index(1))] += 1
    for j in range(M):
        poids[j] /= n
    x = np.linspace(0, 256, 1000)
    y = f_array_tot(x, param, poids)
    plt.plot(x, y)
    affiche_2(X_ech, P, title='etape {}'.format(i))
    plt.savefig('../Resultats/Image_1/etape_finale')
    plt.show()

    print("Nombre d'étapes :", i)
    labels = [P[k].index(1) for k in range(len(X_ech))]

    # X_classe = [int(P[k].index(1)*255/M) for k in range(len(X_ech))]

    # image_finale = np.zeros(shape)
    # for k in range(shape[0]):
    #     image_finale[k] = np.array(X_classe[k*shape[1]:(k+1)*shape[1]])

    # plt.imshow(image_finale)
    # plt.title('image finale')
    # plt.savefig('../Resultats/Image_1/Image_finale')
    # plt.show()


    # In[26]:


    print(poids)
    print(labels[1:10])

    return(labels, param, poids)

# if __name__ == '__main__':
#     file = "../image2.png"
#     X_ech, shape = X_image(file)
#     plt.hist(X_ech, 256, density=True, range=(0, 256), histtype='barstacked')
#     plt.title('Répartition des points')
#     plt.savefig('../Resultats/Image_2/repartition_points')
#     plt.show()
#     n = len(X_ech)

#     labels, param, poids = EBM(X_ech)

# if __name__ == '__main__22':
#     file = "../image2.png"
#     X_ech, shape = X_image(file)
#     plt.hist(X_ech, 256, density=True, range=(0, 256), histtype='barstacked')
#     plt.title('Répartition des points')
#     plt.savefig('../Resultats/Image_2/repartition_points')
#     plt.show()
#     n = len(X_ech)

#     M = 7 # nombres de classes
#     param, P0 = init_EM(M, X_ech)
#     affichage_param(param)
#     affiche_2(X_ech, P0, title='initialisation')
#     poids = M*[0]
#     for j in range(n):
#         poids[int(P0[j].index(1))] += 1
#     for j in range(M):
#         poids[j] /= n
#     x = np.linspace(0, 256, 1000)
#     y = f_array_tot(x, param, poids)
#     plt.plot(x, y)
#     plt.savefig('../Resultats/Image_2/initialisation')
#     plt.show()

#     # ### Réalisation de l'algorithme sur les données

#     # In[24]:


#     methode = 'L-BFGS-B'
#     k = 3500  #  Nombre de points qui doivent changer pour la fonction d'arrêt
#     P_prec = P0
#     param_prec = param[:]
#     i = 0

#     while True:
#         i += 1
#         P0 = etape_E(X_ech, M, param)
#         P = etape_B(X_ech, M, P0)
#         print("\n \n --------------- Etape M n°{} ---------------\n ".format(i))
#         param = etape_M(X_ech, M, param, P)
#         if arret(P_prec, P, param_prec, param, k):
#             break
#         else:
#             plt.clf()
#             P_prec = P
#             param_prec = param[:]
#             affiche_2(X_ech, P, title='etape {}'.format(i))
#             poids = M*[0]
#             for j in range(n):
#                 poids[int(P[j].index(1))] += 1
#             for j in range(M):
#                 poids[j] /= n
#             x = np.linspace(0, 256, 1000)
#             y = f_array_tot(x, param, poids)
#             plt.plot(x, y)
#             plt.savefig('../Resultats/Image_2/etape_{}'.format(i))
#             #plt.show()
#             affichage_param(param)


#     # ## Affichage des résultats

#     # In[25]:


#     affichage_param(param)
#     plt.clf()
#     poids = M*[0]
#     for j in range(n):
#         poids[int(P[j].index(1))] += 1
#     for j in range(M):
#         poids[j] /= n
#     x = np.linspace(0, 256, 1000)
#     y = f_array_tot(x, param, poids)
#     plt.plot(x, y)
#     affiche_2(X_ech, P, title='etape {}'.format(i))
#     plt.savefig('../Resultats/Image_2/etape_finale')
#     plt.show()

#     print("Nombre d'étapes :", i)

#     X_classe = [int(P[k].index(1)*255/M) for k in range(len(X_ech))]

#     image_finale = np.zeros(shape)
#     for k in range(shape[0]):
#         image_finale[k] = np.array(X_classe[k*shape[1]:(k+1)*shape[1]])

#     plt.imshow(image_finale)
#     plt.title('image finale')
#     plt.savefig('../Resultats/Image_2/Image_finale')
#     plt.show()


#     # In[26]:


#     print(poids)

