import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import random

def selection_pixels(file, selection=0.5, affichage=True, m=3):
    image = mpimg.imread(file)

    if image.dtype == np.float32:  # Si le résultat n'est pas un tableau d'entiers
        image = (image * 255).astype(np.uint8)

    if image.shape[0] % m == 0 and image.shape[1] % m == 0:
        I = []
        for k in range(image.shape[0]//m):
            for l in range(image.shape[1]//m):
                square = []
                for i in range(m*k, m*(k+1)):
                    line  = []
                    for j in range(m*l, m*(l+1)):
                        line.append((i,j))

                    square.append(line)

                I.append(square)
        
        n=len(I)
        random.shuffle(I)
        I_select = I[:int(selection*n)]
        I_notselect = I[len(I)-len(I_select):]
        
        I_select_flat = [c for a in I_select for b in a for c in b]

        if affichage:
            plt.scatter(list(map(lambda x: x[0],I_select_flat)), list(map(lambda x: x[1], I_select_flat)))
            plt.show()
        
        val_select_flat = list(map(lambda x: image[x[0], x[1]], I_select_flat))

        return(I_select_flat, I_notselect, val_select_flat)
    else:
        return None

def X_image(file):
    """

    :param file:
    :return:
    """
    img = mpimg.imread(file)

    if img.dtype == np.float32:  # Si le résultat n'est pas un tableau d'entiers
        img = (img * 255).astype(np.uint8)

    # plt.imshow(img)
    # # plt.show()
    #
    # print(img)
    # print(img.shape)

    X = []
    for i in range(img.shape[0]):
        X += list(img[i])

    # print(X)
    # print(len(X))
    return X, img.shape

if __name__ == '__main__':
    file = "image1.png"

    img = mpimg.imread(file)

    if img.dtype == np.float32:  # Si le résultat n'est pas un tableau d'entiers
        img = (img * 255).astype(np.uint8)
    print(img)
    # plt.imshow(img)
    # plt.show()
    X = []
    for i in range(len(img)):
        X.append(img[i])
    print('X = {}'.format(X))
    print('len(X) = {}'.format(len(X)))
    plt.clf()
    X = []
    for i in range(img.shape[0]):
        X += list(img[i])
    plt.hist(X, 256, density=True, range=(0, 256), histtype='barstacked')
    print('plt.show()')
    plt.show()
    
    I_select, I_nonselect, val_select = selection_pixels(file, selection=0.1, m=4)
    print(val_select)
    #affichage des pixels sélectionnés
    #plt.scatter(list(map(lambda x: x[0],I)), list(map(lambda x: x[1], I)))
    #plt.show()